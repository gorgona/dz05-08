/**
 * Created by Админ on 10.08.2016.
 */
/**
 * Created by Elena on 10.08.2016.
 *
 *  Создать калькулятор на асинхронных фукнциях. Будем имитировать долгие вычисления. Реализовать addAsync, multAsync, devideAsync, minusAsync.
 * И с помощью них переводить фаренгейты в цельсии, и наоборот(http://bi0morph.github.io/teaching-materials/lesson-1/exercises/homework.html).

 Например:
 Обычная функция сложения
 function add(a,b) { return a + b; }

 Асинхронная функция сложения:
 function addAsync(a, b, callback) {
  var timeout = Math.random()*10000;
  setTimeout(function() {
    var result = a + b;
    callback(result);
  }, timeout);
 }

 Пример использования:
 addAsync(2, 2, function(result) {
   console.log('Результат: ' + result);
   console.log('Должен быть равен с add(2,2): ', add(2,2) === result);
});

 */

function add(a,b){
    return a+b;
}

function addAsync(a, b, callback) {
    var timeout = Math.random()*10000;
    setTimeout(function() {
        var result = a + b;
        callback(result);
    }, timeout);
}

function addAsyncButton(){
    var a = 2;
    var b = 4;
    console.log(" a = "+a+" b = "+b);
    console.log('add(a,b)= '+add(a,b));
    addAsync(a,b,function(result){
        console.log(' Результат: a+b = '+a+'+'+b+'='+result);
        console.log("Должен быть равен с add("+a+","+b+"): ", add(a,b) === result);
    });
}

function mult(a,b){
    return a * b;
}

function multAsync(a, b, callback) {
    var timeout = Math.random()*10000;
    setTimeout(function() {
        var result = a * b;
        callback(result);
    }, timeout);
}

function multAsyncButton(){
    var a = 2;
    var b = 4;

    console.log(" a = "+a+" b = "+b);
    console.log('mult(a,b)= '+mult(a,b));
    multAsync(a,b,function(result){
        console.log(' Результат: a * b = '+a+'*'+b+'='+result);
        console.log("Должен быть равен с mult("+a+","+b+"): ", mult(a,b) === result);
    });
}

function minus(a,b){
    return a-b;
}

function minusAsync(a, b, callback) {
    var timeout = Math.random()*10000;
    setTimeout(function() {
        var result = a - b;
        callback(result);
    }, timeout);
}

function minusAsyncButton(){
    var a = 12;
    var b = 4;
    console.log(" a = "+a+" b = "+b);
    console.log('minus(a,b)= '+minus(a,b));
    minusAsync(a,b,function(result){
        console.log(' Результат: a+b = '+a+'-'+b+'='+result);
        console.log("Должен быть равен с minus("+a+","+b+"): ", minus(a,b) === result);
    });
}

function devide(a,b){
    return a / b;
}

function devideAsync(a, b, callback) {
    var timeout = Math.random()*10000;
    setTimeout(function() {
        var result = a / b;
        callback(result);
    }, timeout);
}

function devideAsyncButton(){
    var a = 16
    var b = 4;

    console.log(" a = "+a+" b = "+b);
    console.log('devide(a,b)= '+devide(a,b));
    devideAsync(a,b,function(result){
        console.log(' Результат: a * b = '+a+'/'+b+'='+result);
        console.log("Должен быть равен с devide("+a+","+b+"): ", devide(a,b) === result);
    });
}

function FarToCel(f){

    buttonGo.value = 'подождите - думаю';
    minusAsync(f,32,function(result){
        multAsync(result,5,function(result){
            devideAsync(result,9,function(result){
                alert('результат преобразования '+f+' по Фаренгейту : '+result + ' по Цельсию');
                "buttonGo".innerHTML = 'Преобразовать';
                return result;
            })
        })
    })
}

function CelToFar(c){
    "buttonGo".value = 'подождите - думаю';
    multAsync(c,9,function(result){
        devideAsync(result,5,function(result){
            addAsync(result,32,function(result){
                alert('результат преобразования '+c+' по Цельсию : '+result + ' по Фаренгейту');
                "buttonGo".value = 'Преобразовать';
                return result;
            })
        })
    })

}

function buttonGo() {
    var inputNum = inData.value;
    var inputEd = select.value;
    if (inputEd == 's1') {
        FarToCel(inputNum);
    }
    if (inputEd == 's2') {
        CelToFar(inputNum);
    }
}
